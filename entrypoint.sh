#!/bin/bash

EBOT_WEB_HOME='/var/www/html'

EBOT_IP="${EBOT_IP:-}"
EBOT_PORT="${EBOT_PORT:-12360}"

DB_VARS=$(echo $DATABASE_URL | tr ":" "\n")

DB_HOST_NAME=$(echo "$DB_VARS" | tr "@" "\n" | cut -d$'\n' -f4)
DB_USER="mariadb"
DB_PASS=$(echo "$DB_VARS" | tr "@" "\n" | cut -d$'\n' -f3)
DB_PORT=$(echo "$DB_VARS" | tr "/" "\n" | cut -d$'\n' -f6)
DB_NAME=$(echo "$DB_VARS" | tr "/" "\n" | cut -d$'\n' -f7)

MYSQL_HOST="${DB_HOST_NAME:-mysql}"
MYSQL_PORT="${DB_PORT:-3306}"
MYSQL_USER="${DB_USER:-ebotv3}"
MYSQL_PASS="${DB_PASS:-ebotv3}"
MYSQL_DB="${DB_NAME:-ebotv3}"

DEMO_DOWNLOAD="${DEMO_DOWNLOAD:-true}"
DEMO_FOLDER="${DEMOS_DIR:-/opt/ebot/demos}"
LOG_FOLDER="${LOGS_DIR:-/opt/ebot/logs}"

EBOT_ADMIN_USER="${EBOT_ADMIN_USER:-admin}"
EBOT_ADMIN_PASS="${EBOT_ADMIN_PASS:-password}"
EBOT_ADMIN_MAIL="${EBOT_ADMIN_MAIL:-admin@ebot}"

TOORNAMENT_SECRET="${TOORNAMENT_SECRET:-}"
TOORNAMENT_API_KEY="${TOORNAMENT_API_KEY:-}"
TOORNAMENT_PLUGIN_KEY="${TOORNAMENT_PLUGIN_KEY:-azertylol}"

# for usage with docker-compose
while ! nc -z $MYSQL_HOST $MYSQL_PORT; do sleep 3; done

if [ ! -f .installed ]
then
    php $EBOT_WEB_HOME/symfony configure:database "mysql:host=${MYSQL_HOST};dbname=${MYSQL_DB}" $MYSQL_USER $MYSQL_PASS
    php $EBOT_WEB_HOME/symfony doctrine:insert-sql
    php $EBOT_WEB_HOME/symfony guard:create-user --is-super-admin $EBOT_ADMIN_MAIL $EBOT_ADMIN_USER $EBOT_ADMIN_PASS

    # manage config
    sed -i "s|log_match:.*|log_match: ${LOG_FOLDER}/log_match|" $EBOT_WEB_HOME/config/app_user.yml
    sed -i "s|log_match_admin:.*|log_match_admin: ${LOG_FOLDER}/log_match_admin|" $EBOT_WEB_HOME/config/app_user.yml
    sed -i "s|demo_path:.*|demo_path: ${DEMO_FOLDER}|" $EBOT_WEB_HOME/config/app_user.yml
    sed -i "s|ebot_ip:.*|ebot_ip: ${EBOT_IP}|" $EBOT_WEB_HOME/config/app_user.yml
    sed -i "s|ebot_port:.*|ebot_port: ${EBOT_PORT}|" $EBOT_WEB_HOME/config/app_user.yml
    sed -i "s|demo_download:.*|demo_download: ${DEMO_DOWNLOAD}|" $EBOT_WEB_HOME/config/app_user.yml
    sed -i "s|toornament_id:.*|toornament_id: ${TOORNAMENT_ID}|" $EBOT_WEB_HOME/config/app_user.yml
    sed -i "s|toornament_secret:.*|toornament_secret: ${TOORNAMENT_SECRET}|" $EBOT_WEB_HOME/config/app_user.yml
    sed -i "s|toornament_api_key:.*|toornament_api_key: ${TOORNAMENT_API_KEY}|" $EBOT_WEB_HOME/config/app_user.yml
    sed -i "s|toornament_plugin_key:.*|toornament_plugin_key: ${TOORNAMENT_PLUGIN_KEY}|" $EBOT_WEB_HOME/config/app_user.yml

    touch .installed
fi

php $EBOT_WEB_HOME/symfony cc

apache2-foreground
