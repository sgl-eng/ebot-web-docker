FROM php:5.6.25-apache

ENV TIMEZONE="Europe/Paris" EBOT_HOME="/opt/ebot" DEMOS_DIR="${EBOT_HOME}/demos" LOGS_DIR="${EBOT_HOME}/logs"

RUN apt-get update -y
RUN apt-get install -y git netcat
RUN mkdir -p ${DEMO_DIRS} ${LOGS_DIR}
RUN a2enmod rewrite
RUN docker-php-ext-install pdo_mysql
RUN echo 'date.timezone = "${TIMEZONE}"' >> /usr/local/etc/php/conf.d/php.ini
RUN rm -rf /var/www/html
RUN git clone https://github.com/deStrO/eBot-CSGO-Web.git /tmp/html

WORKDIR /tmp/html
ENV TAG="master"

RUN git checkout $TAG
RUN cp config/app_user.yml.default config/app_user.yml
RUN rm -rf .git

WORKDIR /

# Workaround: https://github.com/docker/docker/issues/4570
RUN mv /tmp/html /var/www
RUN rm -rf /var/www/html/web/installation

RUN mkdir -p ${EBOT_HOME} ${DEMOS_DIR} ${LOGS_DIR}
RUN chown www-data:www-data -R /var/www "${EBOT_HOME}"

RUN sed -i 's@#RewriteBase /@RewriteBase /@g' /var/www/html/web/.htaccess

COPY 000-default.conf /etc/apache2/sites-available/000-default.conf

COPY entrypoint.sh /sbin/entrypoint.sh

RUN chmod +x /sbin/entrypoint.sh

EXPOSE 80

ENTRYPOINT ["/sbin/entrypoint.sh"]
